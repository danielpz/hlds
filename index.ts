import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import fs from 'fs';
import path from 'path';
import vhost from 'vhost';

const hl = express()
  .use(morgan('combined', {
    stream: fs.createWriteStream(path.join(__dirname, './logs/hl.log'), { flags: 'a' })
  }))
  .use(helmet())
  .use('/', express.static('./public/hl'));

const cs = express()
  .use(morgan('combined', {
    stream: fs.createWriteStream(path.join(__dirname, './logs/cs.log'), { flags: 'a' })
  }))
  .use(helmet())
  .use('/', express.static('./public/cs'))

express()
  .use(vhost('hl.dannyhost.com', hl))
  .use(vhost('cs.dannyhost.com', cs))
  .listen(80, () => {
    console.info(`Server is up and running @ http://localhost:80`);
  });
